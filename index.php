<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

    
    <!-- Basic Syntax -->

    <?php
    echo "Hello, This is Farhan Labib.\n";
    echo 55+885;
    echo "\nI was the student of Daffodil Internation University<br>";
?>

   

   <?php
    
    $name = 'Farhan Labib';
    $age = 24;
    echo "My name is $name and I'm $age years old<br>";

    $var1=25;
    $var2=15;
    echo "Total Sum: ";
    echo $var1+$var2; 

    echo "<br>Total Sum: ";
    echo $var1-$var2; 

    echo "<br>Total Sum: ";
    echo $var1*$var2; 

    echo "<br>Total Sum: ";
    echo $var1/$var2; 
    ?>

    <?php 
    echo "<br>single line comment<br>";  //single line

    echo "multiple line comment<br>";  /* multile 
                                       line */
    ?>



    <!-- Types -->


    <?php
    echo "<br>";
    $var_bool=true;
    $var_str="Labib";
    $var_int=20;

    echo get_debug_type($var_bool), "<br>";
    echo get_debug_type($var_str), "<br>";

    if(is_int($var_int))
    {
        $var_int+=4;
    }

    var_dump($var_int);    // var_dump => var type($var_int)

    if(is_string($var_str))
    {
        echo "<br>String: $var_str<br>";
    }

    $var_nl=NULL;

    if(is_null($var_nl))
    {
        echo "This variable is empty<br>";
    }
    



?>

<!-- string -->

<?php 

echo <<<EOT

       a
     b  
   c

EOT;
echo "<br>";
?>

<?php
var_dump("0D1" == "000");
echo "<br>";
var_dump("2E1" == "020");
echo "<br>"; 
?>

<!-- array -->

<?php
$arr = array("Sayed", "Farhan", "Labib", "Karib");
echo var_dump($arr);
echo "<br>";
echo $arr[0];
echo "<br>";
echo $arr[3];

?>

<!-- Constant -->

<?php

define("CONSTANT", "Labib");
echo "<br>";
echo CONSTANT;
echo "<br>";

define("pi", 3.14159);
echo "<br>";
echo pi;
echo "<br>";




?>
<?php
$a = 15;
$b = 25;

function Sum()
{
    global $a, $b, $total;
    $total = $a + $b;
} 

Sum();
echo $total; 
echo "<br>";
?>

<?php
$a = 1;
$b = 2;
$c = 3;

function itteration()
{
    global $a, $b, $c;
    $a++;
    $a+=$b;
    $a--;
    $a+=$c;
    $a++;

    echo $a;
} 

itteration();

echo "<br>";
?>

<?php
$a = 10;
$b = 5;

if($a>$b) echo "a is the large number<br>";
else echo "b is the large number<br>";

$year_2023 = false;

if($year_2023) echo "Leap Year<br>";
else echo "Go forward<br>";


$age = 24;

echo ($age>18) ? "You are adult<br>" : "You are little<br>";



?>


<?php
$animal = "cow";
switch($animal)
{
    case "monkey":
        echo "Monkey has tail<br>";
        break;
    
    case "cow":
        echo "Cow gives us milk<br>";
        break;
        
    case "snake":
        echo "Snake moves like zigzag<br>";    
        break;
}

?>

<?php

$count_of = 17;
for($i=1;$i<=10;$i++)
{
    $co=$i*$count_of;
    echo "17 X ", $i, " = ", $co;
    echo "<br>";
}

echo "While Loop <br>";

$c=15;
$i=1;

while($i*$c<=105)
{
    echo "15 X ", $i, " = ", $i * $c;
    echo "<br>";
    $i++;
}

?>
<?php

echo "Foreach Loop<br>";

$animals = ['cow', 'tiger', 'lion', 'bear', 'dog', 'cat'];



foreach($animals as $animal)
{
    echo $animal;
    echo "<hr />";
}

$animals = [
    'animal1' => 'cow', 
    'animal2' => 'tiger',
    'animal3' => 'lion'];

    foreach($animals as $key=>$animal)
{
    if($animal=='tiger')
   {
    continue;
   }
    echo $key, ' ', $animal;
    echo "<hr />";
}

array_flip($animals);


?>

<!-- String -->

<?php

echo "String practice<br>";

$name = "Labib";
$str = "My Name is $name";
echo "Total Characters are: ", strlen($str);
echo "<br>";
echo trim($str);
echo "<br>";
echo strtolower($str);
echo "<br>";
echo strtoupper($str);
echo "<br>";
echo "Total Words are: ", str_word_count($str);
echo "<br>";

$lname = 'Karib';
$fname = "Labib $lname";
echo $fname;
echo "<br>";

$name = " Farhan ";
echo str_repeat($name, 3);
echo "<br>";
echo trim($name);
echo "<br>";

$str = ['Farhan','Labib'];
echo implode(">",$str);
echo "<br>";
echo implode(" ",$str);
echo "<br>";
 $name = "Farhan Labib";
 echo str_replace("Farhan","Karib",$name);
 echo "<br>";

 echo serialize($name);
 echo "<br>";
 $var = 1002.55;
 echo serialize($var);
 echo "<br>";
$arr = ['5','54','35','12'];
$var = array_sum($arr);
echo $var;
echo "<br>";

$player = ['Messi', 'Dhoni', 'Son'];
$game = ['Football', 'Cricket', 'Football'];

array_map(function($player, $game){
    return "$player is a $game player\n";
},$player, $game);

?>

<?php

$stack = ['cow', 'tiger', 'lion', 'bear', 'dog', 'cat'];

$animals = array_pop($stack);

echo $animals;
echo "<br>";
print_r($stack);

$animals = array_push($stack, 'husky', 'fox');
echo "<br>";
print_r($stack);

?>

<?php
echo "<br>";

$str = "Sayed Farhan Labib Karib";
$pattern = "/Farhan|Karib/i";
echo  preg_match_all($pattern, $str, $matches);
print_r($matches);
echo "<br>";

$array = array(1,2,3,5,8,13,21,34,55);
$s=0;
for($i=0;$i<5;$i++)
{
    $s+=$array[$array[$i]];
}
echo $s;

?>

<!-- Function -->

<?php
echo "<br>";

function message()
{
    echo "Hello, Labib!";
}

message();
echo "<br>";

function squre($n)
{
    return $n*$n;
}

echo squre(4);
echo "<br>";
echo squre(15);
echo "<br>";

$rest = substr("abcdef", -1);
echo $rest;
echo "<br>";
$rest = substr("abcdef", -2);
echo $rest;
echo "<br>";
$rest = substr("abcdef", -3);
echo $rest;
echo "<br>";
$rest = substr("abcdef", -4);
echo $rest;
echo "<br>";
$rest = substr("abcdef", -3, 1);
echo $rest;
echo "<br>";
$rest = substr("abcdef", -3, 2);
echo $rest;
echo "<br>";
$rest = substr("abcdef", -3, 3);
echo $rest;
echo "<br>";

// by build in function 

function mul()
{

    $n=func_get_args();
    $t=1;
    for($i=0;$i<count($n);$i++)
    {
        $t*=$n[$i];
    }
    return $t;
   
}
echo mul(10, 5) . "<br>";
echo mul(10, 5, 7) . "<br>";




?>



<?php

// Recursive function

function fact($n)
{

    if($n<0) return -1;
    else if($n==0) return 1;
    else return ($n*fact($n-1));

}
echo fact(5);
echo "<br>";
echo fact(7);
echo "<br>";
echo fact(9);
echo "<br>";
// date time

$date = new DateTime();
echo $date -> getTimestamp();


?>


</body>
</html>




